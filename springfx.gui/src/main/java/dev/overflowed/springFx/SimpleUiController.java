package dev.overflowed.springFx;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import org.springframework.stereotype.Component;

@Component
public class SimpleUiController {

    private final IntService intService;

    @FXML
    private Label label;

    @FXML
    private Button button;

    public SimpleUiController(IntService intService) {
        this.intService = intService;
    }

    @FXML
    public void initialize() {
        this.button.setOnAction(actionevent -> this.label.setText(String.valueOf(intService.getInt())));
    }

}
