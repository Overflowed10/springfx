package dev.overflowed.springFx;

import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class DefaultIntService implements IntService {
    private Random r = new Random();

    @Override
    public int getInt() {
        return r.nextInt(11);
    }
}
